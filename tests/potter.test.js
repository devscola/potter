const checkout = require('../potter')

describe('Potter', () => {
  it('says the price of a book', () => {
    const book = 'Stone'

    const total = checkout([book])

    expect(total).toEqual(8)
  })

  it('sums the price of two same books', () => {
    const book = 'Stone'

    const total = checkout([book, book])

    expect(total).toEqual(16)
  })

  it('applies a 5% discount when buying 2 different books', () =>{
    const books = ['Stone', 'Chamber']

    const total = checkout(books)

    expect(total).toEqual(16 - 0.8)
  })

  it('applies a 10% discount when buying 3 different books', () =>{
    const books = ['Stone', 'Chamber', 'Prisioner']

    const total = checkout(books)

    expect(total).toEqual(24 - 2.4)
  })

  it('considers all 3 books before applying a discount', () =>{
    const books = ['Stone', 'Chamber', 'Stone']

    const total = checkout(books)

    expect(total).toEqual(24 - 0.8)
  })

  it('no books is free', () => {
    const total = checkout([])

    expect(total).toEqual(0)
  })
})

