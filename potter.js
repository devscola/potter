const pricePerBook = 8
const twoDifferentBooksDiscount = 0.95
const threeDifferentBooksDiscount = 0.90
const noDiscount = 1

const checkout = (books) => {
  let discount = noDiscount
  if (hasDifferentBooks(books, 2)) {
    discount = twoDifferentBooksDiscount
  }
  if (hasDifferentBooks(books, 3)) {
    discount = threeDifferentBooksDiscount
  }
  return books.length * pricePerBook * discount
}

const hasDifferentBooks = (books, count) => {
  return areDifferent(books, count)
}

const areDifferent = (books, count) => {
  const unique = books.reduce((acc, current) => {
    acc[current] = true
    return acc
  }, {})
  return Object.keys(unique).length === count
  return books[0] !== books[1]
}

module.exports = checkout
